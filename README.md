# csa-lab3

- P33082, Панин Иван Михайлович
- `forth | stack | neum | hw | instr | binary | trap | mem | pstr | prob1 | spi`
- без усложнения

# Язык программирования
- Реализован forth подобный язык, транслирующийся в ассемблер. 
Язык следует обратной польской нотации.
```ebnf
<set IO address> := <address> "$io"

<program> ::= (<function>)+ (<term>)+

<term> ::= <comment> | <command> | <loop> | <print>

<command> ::= "+" | "%" | "swap" | "." | "<<" | "otop" | "ztop" | "last" | "puf" | "lcm" | "pus" | ">" | "^"

<bool_command> ::= "ztop" | "last" | "otop"

<loop> ::= "begin" <bool_command> "while" (<command>)+ "repeat"

<name> ::= [a-zA-Zа-яА-Я!?_]+

<function> ::= ":" <name> <comment> (<command> | <loop>)+ ";"

<comment> ::= "(" [a-z]+ ")"

<load const> ::= <value> "<"

<write memory> ::= <address> "^"

<read memory> ::= <address> "<<"

```
В начале программы идёт объявление всех прерываний и функций. После этого задаётся набор команд и вызовов функций, выполняющийся последовательно.

# Организация памяти

```
			IVT
+-------------------------+
|00   Interrupt vector #0 |
|            ...          |
|07   Interrupt vector #1 |
+-------------------------+

	Instruction memory
+-------------------------+
|08 program start	        |
|            ... 		      |
+-------------------------+

		Data memory
+-------------------------+
|   Data start  	        |
|            ... 		      |
|FF Data end              |
+-------------------------+		

Размер машинного слова - 64 бита.
Адресное пространство 256 байт
Поддерживается непосредственная адресация (адрес как операнд инструкции)
Автоматическая память, используемая для констант может быть переиспользована
```

Отсутствует система типов.

# Система команд

Особенности процессора:

- Стековая архитектура. 

- Ширина инструкций - 16 бит. первые 8 бит отданы под кодирование инструкции, вторые 8 бит - под операнд, при его наличии. В случае отсутствия операнда, второй байт инструкции игнорируется. 

- Ввод-вывод реализован с помощью MMIO. Программист может выбрать адрес, который будет использоваться для ввода-вывода данной программы. 

- Для ввода-вывода при запуске control_unit передаются пути к файлам для in и out. В in файле в начале пишется такт, в который придёт input, после чего само значение. 

- Реализованы прерывания. Определяются программистом как функции, носящие поределённое имя. Вызов прерываний происходит с помощью IVT. Нулевое прерывание срабатывает при вводе данных MMIO. 

- Присутствует 3 внутренних стека. Стек значений, заменяющий регистры, стек возвратных адресов, на котором функции хранят свой возвратный адрес, стек возвратных адресов прерываний, на котором прерывания хранят свои возвратные адреса. 

-Таблица команд:

| Обозначение | Аргумент | Описание |
| ----------- | -------- | -------- |
| HALT        | -        | Останавливает программу |
| ADD         | -        | Складывает два верхних числа на стеке |
| MOD         | -        | Находит остаток от деления второго значения на стеке от верхнего |
| SWAP        | -        | Меняет местами два верхних элемента стека |
| ZTOP        | -        | Кладёт на стек 0 если верхний элемент = 0, иначе 1 |
| LAST        | -        | Кладёт на стек 0 если верхний элемент единственный, иначе 1 |
| INT         | Номер прерывания | Вызывает прерывание по адресу из таблицы прерываний |
| OTOP        | -        | Кладёт на стек 0 если верхний элемент = 1, иначе 0 |
| STORE       | Адрес операнда | Записывает в память верхний элемент со стека |
| RETURN      | -        | Совершает переход по верхнему адресу со стека возвратных значений |
| PUS         | -        | Кладёт на стек второй по счёту элемент |
| LCM         | -        | Находит наименьший общий делитель двух верхних элементов стека |
| CALL        | Адрес функции  | Вызывает функцию по адресу |
| JZ          | Адрес перехода | Условный переход по адресу если на вершине стека 0 |
| JUMP        | Адрес перехода | Безусловный переход по адресу |
| READ_ADR    | Адрес операнда | Кладёт на стек значение, находящееся по адресу |
| DIV         | -              | Делит второй по счёту элемент на верхний |
| PUF         | -              | Дублирует верхний элемент стека |
| DEC         | -              | Уменьшает на 1 верхний элемент стека |
| EI          | -              | Включает прерывания |
| MUL         | -              | Находит произведение двух верхних элементов стека |
| NBL         | -              | Переводит число в логическое представление и применяет NOT |
| BL          | -              | Переводит число в логическое представление |
| EMPTY       | -              | Кладёт 0 на стек если стек пуст, иначе 1 |
| DI          | -              | Отключает прерывания |
| INC         | -              | Увеличивает значение TOS на 1 |
| RDPB        | -              | считывает на вершину стека 8-битное значение используя TOS как адрес, меняет TOS местами, чтобы указатель снова стал TOS |
| RDPW        | -              | считывает на вершину стека 64-битное значение используя TOS как адрес, меняет TOS местами, чтобы указатель снова стал TOS |        
| WRPB        | -              | Записывает 8-битное значение по указателю в память |
| WRPW        | -              | Записывает 64-битное значение по указателю в память |
| NOP         | -              | Нет операции |

# Транслятор

Команда для запуска транслятора:
```
python3 trans.py SOURCE DEBUG BINARY
```

Транслятор получает на вход исходный код программы, записывает его в промежуточный файл с ассемблерным кодом, после чего преобразует код на ассемблере в бинарный вид и записывает результат в исполняемый файл. 

# Модель процессора
Процессор реализован в двух модулях - [Control unit](https://gitlab.se.ifmo.ru/adivanced/csa_ak_2023_lab3/-/blob/master/lab3/control_unit.py) и [Data path](https://gitlab.se.ifmo.ru/adivanced/csa_ak_2023_lab3/-/blob/master/lab3/datapath.py)

Команда для запуска модели процессора:
```
python3 control_unit.py BINARY INPUT OUTPUT
```

### Control Unit 

<img src="img/control_unit.png" alt="drawing"/>

Hardwired.
Моделирование на уровне инструкций.
Достает команду из памяти по относительному адресу, который хранится в Program Counter, декодирует ее.
Декодирование происходит с помощью сопоставления кода и порядкового номера команды. Для адресных команд
используется маска, позволяющая определить тип перехода и аргумент (относительный адрес). В зависимости от типа
адресной команды, адрес возврата может быть положен в стек возврата (для команды call) или в
program counter (для jmp, jz)
После декодирования инструкции в DataPath посылается один из сигналов:

``` python
class Signal(enum.IntEnum):
    WRITE = 0
    CHECK_ONE = 1
    CHECK_NOT_ZERO = 2
    CHECK_NOT_LAST = 3
    SWAP = 4
    MOD = 5
    ADD = 6
    READ = 7
    PUS = 8
    LCM = 9
    DIV = 10
    PUF = 11
    DEC = 12
    EI = 13
    MUL = 14
    NBL = 15
    BL = 16
    EMPTY = 17
    HALT = 18
    JZ = 19
    JUMP = 20
    CALL = 21
    RETURN = 22
    INT = 23
    DI = 24
    RDPW = 25
    RDPB = 26
    WRPW = 27
    WRPB = 28
    INC = 29
    NOP = 50
```
Когда была декодирована команда halt, работа процессора останавливается.

### DataPath

<img src="img/datapath.png" alt="drawing"/>

Получает сигнал от CU и обрабатывает его. Все манипуляции с элементами происходят в TOS (верхние три элемента стека),
что позволяет исполнять все операции за один такт.
В регистре stack pointer хранится указатель на верх стека, который изменяется после манипуляций со стеком.
С TOS связана память, откуда в стек могут записываться данные.




# Тестирование 
Для демонстрации работы представлено 4 программных комплекса
1. cat
2. hello
3. hello_username
4. prob1

Для каждой программы выполняются тесты трансляции кода в ассемблер
После чего выполняются интеграционные тесты 

Тесты размещены в файле [test_.py](https://gitlab.se.ifmo.ru/adivanced/csa_ak_2023_lab3/-/blob/master/test/test_.py)

CI при помощи gitlab:

``` yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format .
```

где:

- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

pyproject.toml - настройки зависимостей

Пример использования на примере программы prob1:

Исходный код:
```
200 $io
: int0 200 ^ ei ;
nop
nop
120 <<
128 <<
dec
begin ztop while puf 120 ^ % pus 128 ^ % * nbl pus * 136 ^ + 136 << dec repeat
136 ^
begin ztop while puf 10 < % 48 < + 200 << 10 < / repeat

```

Входные данные:
```
1 1000
3 3
6 5

```

Выходные данные:
```
8
6
1
3
3
2
```
Таким образом, ответ на prob1 = 233168, что можно нетрудно проверить. 

Пример лога, выдаваемого при работе программы(ввиду размера лога, часть его пропущена): 
```
  TICK:  0  FETCHED:  nop 0 0xff00  PC:  16  STACK: < TOP
  TICK:  1  FETCHED:  read 200 0x10c8  PC:  10  STACK: < TOP
  TICK:  2  FETCHED:  ei 0 0x1400  PC:  12  STACK: 1000 < TOP
  TICK:  3  FETCHED:  read 200 0x10c8  PC:  10  STACK: 1000 < TOP
  TICK:  4  FETCHED:  ei 0 0x1400  PC:  12  STACK: 1000 3 < TOP
  TICK:  5  FETCHED:  ret 0 0x0900  PC:  14  STACK: 1000 3 < TOP
  TICK:  6  FETCHED:  read 200 0x10c8  PC:  10  STACK: 1000 3 < TOP
  TICK:  7  FETCHED:  ei 0 0x1400  PC:  12  STACK: 1000 3 5 < TOP
  TICK:  8  FETCHED:  ret 0 0x0900  PC:  14  STACK: 1000 3 5 < TOP
  TICK:  9  FETCHED:  ret 0 0x0900  PC:  14  STACK: 1000 3 5 < TOP
  TICK:  10  FETCHED:  nop 0 0xff00  PC:  18  STACK: 1000 3 5 < TOP
  TICK:  11  FETCHED:  store 120 0x0878  PC:  20  STACK: 1000 3 5 < TOP
  TICK:  12  FETCHED:  store 128 0x0880  PC:  22  STACK: 1000 3 < TOP
  TICK:  13  FETCHED:  dec 0 0x1300  PC:  24  STACK: 1000 < TOP
  TICK:  14  FETCHED:  ztop 0 0x0400  PC:  26  STACK: 999 < TOP
  TICK:  15  FETCHED:  jz 58 0x0d3a  PC:  28  STACK: 999 1 < TOP
  TICK:  16  FETCHED:  puf 0 0x1200  PC:  30  STACK: 999 < TOP
  TICK:  17  FETCHED:  read 120 0x1078  PC:  32  STACK: 999 999 < TOP
  TICK:  18  FETCHED:  mod 0 0x0200  PC:  34  STACK: 999 999 5 < TOP
  TICK:  19  FETCHED:  pus 0 0x0a00  PC:  36  STACK: 999 4 < TOP
  TICK:  20  FETCHED:  read 128 0x1080  PC:  38  STACK: 999 4 999 < TOP
  TICK:  21  FETCHED:  mod 0 0x0200  PC:  40  STACK: 999 4 999 3 < TOP
  TICK:  22  FETCHED:  mul 0 0x1500  PC:  42  STACK: 999 4 0 < TOP
  TICK:  23  FETCHED:  nbl 0 0x1600  PC:  44  STACK: 999 0 < TOP
  TICK:  24  FETCHED:  pus 0 0x0a00  PC:  46  STACK: 999 1 < TOP
  TICK:  25  FETCHED:  mul 0 0x1500  PC:  48  STACK: 999 1 999 < TOP
  TICK:  26  FETCHED:  read 136 0x1088  PC:  50  STACK: 999 999 < TOP
  .
  .
  .
    TICK:  17003  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 233168 233168 < TOP
  TICK:  17004  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 233168 233168 10 < TOP
  TICK:  17005  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 233168 8 < TOP
  TICK:  17006  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 233168 8 48 < TOP
  TICK:  17007  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 233168 56 < TOP
  TICK:  17008  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 233168 < TOP
  TICK:  17009  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 233168 10 < TOP
  TICK:  17010  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 23316 < TOP
  TICK:  17011  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 23316 < TOP
  TICK:  17012  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 23316 1 < TOP
  TICK:  17013  FETCHED:  puf 0 0x1200  PC:  66  STACK: 0 23316 < TOP
  TICK:  17014  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 23316 23316 < TOP
  TICK:  17015  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 23316 23316 10 < TOP
  TICK:  17016  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 23316 6 < TOP
  TICK:  17017  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 23316 6 48 < TOP
  TICK:  17018  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 23316 54 < TOP
  TICK:  17019  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 23316 < TOP
  TICK:  17020  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 23316 10 < TOP
  TICK:  17021  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 2331 < TOP
  TICK:  17022  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 2331 < TOP
  TICK:  17023  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 2331 1 < TOP
  TICK:  17024  FETCHED:  puf 0 0x1200  PC:  66  STACK: 0 2331 < TOP
  TICK:  17025  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 2331 2331 < TOP
  TICK:  17026  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 2331 2331 10 < TOP
  TICK:  17027  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 2331 1 < TOP
  TICK:  17028  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 2331 1 48 < TOP
  TICK:  17029  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 2331 49 < TOP
  TICK:  17030  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 2331 < TOP
  TICK:  17031  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 2331 10 < TOP
  TICK:  17032  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 233 < TOP
  TICK:  17033  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 233 < TOP
  TICK:  17034  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 233 1 < TOP
  TICK:  17035  FETCHED:  puf 0 0x1200  PC:  66  STACK: 0 233 < TOP
  TICK:  17036  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 233 233 < TOP
  TICK:  17037  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 233 233 10 < TOP
  TICK:  17038  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 233 3 < TOP
  TICK:  17039  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 233 3 48 < TOP
  TICK:  17040  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 233 51 < TOP
  TICK:  17041  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 233 < TOP
  TICK:  17042  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 233 10 < TOP
  TICK:  17043  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 23 < TOP
  TICK:  17044  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 23 < TOP
  TICK:  17045  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 23 1 < TOP
  TICK:  17046  FETCHED:  puf 0 0x1200  PC:  66  STACK: 0 23 < TOP
  TICK:  17047  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 23 23 < TOP
  TICK:  17048  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 23 23 10 < TOP
  TICK:  17049  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 23 3 < TOP
  TICK:  17050  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 23 3 48 < TOP
  TICK:  17051  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 23 51 < TOP
  TICK:  17052  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 23 < TOP
  TICK:  17053  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 23 10 < TOP
  TICK:  17054  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 2 < TOP
  TICK:  17055  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 2 < TOP
  TICK:  17056  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 2 1 < TOP
  TICK:  17057  FETCHED:  puf 0 0x1200  PC:  66  STACK: 0 2 < TOP
  TICK:  17058  FETCHED:  read 100 0x1064  PC:  68  STACK: 0 2 2 < TOP
  TICK:  17059  FETCHED:  mod 0 0x0200  PC:  70  STACK: 0 2 2 10 < TOP
  TICK:  17060  FETCHED:  read 108 0x106c  PC:  72  STACK: 0 2 2 < TOP
  TICK:  17061  FETCHED:  add 0 0x0100  PC:  74  STACK: 0 2 2 48 < TOP
  TICK:  17062  FETCHED:  store 200 0x08c8  PC:  76  STACK: 0 2 50 < TOP
  TICK:  17063  FETCHED:  read 100 0x1064  PC:  78  STACK: 0 2 < TOP
  TICK:  17064  FETCHED:  div 0 0x1100  PC:  80  STACK: 0 2 10 < TOP
  TICK:  17065  FETCHED:  jmp 60 0x0e3c  PC:  82  STACK: 0 0 < TOP
  TICK:  17066  FETCHED:  ztop 0 0x0400  PC:  62  STACK: 0 0 < TOP
  TICK:  17067  FETCHED:  jz 82 0x0d52  PC:  64  STACK: 0 0 0 < TOP
  TICK:  17068  FETCHED:  halt 0 0x0000  PC:  84  STACK: 0 0 < TOP
```
Пример работы тестов:
``` powershell
  on backup-shared-runner B8XJyBdS
Preparing the "docker" executor 00:02
Using Docker executor with image ryukzak/python-tools ...
Using docker image sha256:1bfeaf60b074a99a24bf02508072ade11d33013e680e88425cd346693fc5e409 for ryukzak/python-tools with digest ryukzak/python-tools@sha256:2e3ac332bc8e9e61c31e904badebfb4820fd284ede9b74b1e2db287cd732a5a2 ...
Preparing environment 00:00
Running on runner-b8xjybds-project-20457-concurrent-0 via blog...
Getting source from Git repository 00:02
Fetching changes with git depth set to 50...
Reinitialized existing Git repository in /builds/adivanced/csa_ak_2023_lab3/.git/
Checking out a749c3ef as master...
Removing .coverage
Removing .pytest_cache/
Removing .ruff_cache/
Removing lab3/__pycache__/
Removing poetry.lock
Removing test/__pycache__/
Skipping Git submodules setup
Executing "step_script" stage of the job script 00:20
Using docker image sha256:1bfeaf60b074a99a24bf02508072ade11d33013e680e88425cd346693fc5e409 for ryukzak/python-tools with digest ryukzak/python-tools@sha256:2e3ac332bc8e9e61c31e904badebfb4820fd284ede9b74b1e2db287cd732a5a2 ...
$ poetry install
Skipping virtualenv creation, as specified in config file.
Updating dependencies
Resolving dependencies...
Package operations: 0 installs, 6 updates, 0 removals
  • Updating pytest (7.4.3 -> 7.4.4)
  • Updating ruamel-yaml (0.18.3 -> 0.18.5)
  • Updating typing-extensions (4.8.0 -> 4.9.0)
  • Updating coverage (7.3.2 -> 7.4.0)
  • Updating mypy (1.6.1 -> 1.8.0)
  • Updating ruff (0.1.3 -> 0.1.14)
Writing lock file
$ coverage run -m pytest --verbose
============================= test session starts ==============================
platform linux -- Python 3.12.0, pytest-7.4.4, pluggy-1.3.0 -- /usr/local/bin/python
cachedir: .pytest_cache
rootdir: /builds/adivanced/csa_ak_2023_lab3
configfile: pyproject.toml
testpaths: test
plugins: golden-0.2.2
collecting ... collected 4 items
test/test_.py::test_prob1[../golden/prob1.yml] PASSED                    [ 25%]
test/test_.py::test_cat[../golden/cat.yml] PASSED                        [ 50%]
test/test_.py::test_hello[../golden/hello.yml] PASSED                    [ 75%]
test/test_.py::test_hello_username[../golden/hello_username.yml] PASSED  [100%]
============================== 4 passed in 4.58s ===============================
$ find . -type f -name "*.py" | xargs -t coverage report
coverage report ./test/test_.py ./lab3/common.py ./lab3/trans.py ./lab3/control_unit.py ./lab3/datapath.py
Name                     Stmts   Miss  Cover
--------------------------------------------
./lab3/common.py            76      0   100%
./lab3/control_unit.py     170     20    88%
./lab3/datapath.py         212     73    66%
./lab3/trans.py            186     27    85%
./test/test_.py            107      0   100%
--------------------------------------------
TOTAL                      751    120    84%
$ ruff format .
5 files reformatted
Cleaning up project directory and file based variables 00:01
Job succeeded
```

Статистика:
```
| ФИО                   | <алг>            | code size | <code инстр.> | <такт.> | <вариант>                                                          |
|-----------------------|------------------|-----------|---------------|---------|--------------------------------------------------------------------|
| Панин Иван Михайлович | cat              | 101       | 16            | 20      | forth, stack, neum, hw, instr, binary, trap, mem, pstr, prob1, spi |
| Панин Иван Михайлович | prob1            | 117       | 38            | 17068   | forth, stack, neum, hw, instr, binary, trap, mem, pstr, prob1, spi |
| Панин Иван Михайлович | hello            | 117       | 5             | 5       | forth, stack, neum, hw, instr, binary, trap, mem, pstr, prob1, spi |
| Панин Иван Михайлович | hello_username   | 117       | 13            | 13      | forth, stack, neum, hw, instr, binary, trap, mem, pstr, prob1, spi |
