import enum
import sys
import struct
import time

from lab3.common import Command, Offsets, Signal, instrnames

from lab3.datapath import Data_path

from lab3.common import HaltProgram, InputTokensEnd, SegmentOverflow

class Control_Unit:
	def __init__(self, datapath):
		self.data_path = datapath
		self.tick = 0

	def tickk(self):
		self.tick += 1
		if self.data_path.in_int == 1:
			return
		resf, resv = self.data_path.check_tick(self.tick)
		if resf:
			#print("GOT INPUT!!!!", resv)
			if not isinstance(resv, bytes):
				barr = bytearray(resv.to_bytes(8, 'big'))
			else:
				barr = bytearray(resv)
			#print(barr)
			#print(barr[0], barr[1], barr[2], barr[3], barr[4], barr[5], barr[6], barr[7])
			self.data_path.memory[self.data_path.ioadr] = int(barr[0])
			self.data_path.memory[self.data_path.ioadr+1] = int(barr[1])
			self.data_path.memory[self.data_path.ioadr+2] = int(barr[2])
			self.data_path.memory[self.data_path.ioadr+3] = int(barr[3])
			self.data_path.memory[self.data_path.ioadr+4] = int(barr[4])
			self.data_path.memory[self.data_path.ioadr+5] = int(barr[5])
			self.data_path.memory[self.data_path.ioadr+6] = int(barr[6])
			self.data_path.memory[self.data_path.ioadr+7] = int(barr[7])
			if self.data_path.memory[0] != 0:
				self.data_path.in_int = 1
				self.data_path.ret_stack.append(self.data_path.pc)
				self.data_path.pc = self.data_path.memory[0]




	def send_signal(self, signal, addr):
		self.data_path.handle_signal(signal, addr)

	def start(self):
		cur_instr = 0
		limit = 50000
		instr_cnt = 0

		while True:
			self.command_cycle()
			instr_cnt += 1


	def command_cycle(self):
		#time.sleep(0.05)
		#print("Stack: ", end="")
		#for i in self.data_path.stack:
		#	print(" ", i, end="")
		#print(" <TOP")
		#time.sleep(0.05)

		try:
			self.data_path.latch_pc()
		except SegmentOverflow as segment_fault:
			logging.error("PC REACHED DATA SEGMENT!")
			raise HaltProgram() from segment_fault
		tck = self.tick
		cur_instruction = self.data_path.get_next_instruction(tck)
		match cur_instruction&0xFF00:
			case Command.HALT:
				#print("HALT!, PC: ", self.data_path.pc, " SP: ", self.data_path.sp)
				self.send_signal(Signal.HALT, 0)
				#raise HaltProgram()
			case Command.OTOP:
				self.send_signal(Signal.CHECK_ONE, 0)
			case Command.ZTOP:
				self.send_signal(Signal.CHECK_NOT_ZERO, 0)
			case Command.LAST:
				self.send_signal(Signal.CHECK_NOT_LAST, 0)
			case Command.JZ:
				self.send_signal(Signal.JZ, cur_instruction&0x00FF)
			case Command.JUMP:
				self.send_signal(Signal.JUMP, cur_instruction&0x00FF)
			case Command.CALL:
				self.send_signal(Signal.CALL, cur_instruction&0x00FF)
			case Command.READ_ADR:
				self.send_signal(Signal.READ, cur_instruction&0x00FF)
			case Command.STORE:
				self.send_signal(Signal.WRITE, cur_instruction&0x00FF)
			case Command.PUS:
				self.send_signal(Signal.PUS, 0)
			case Command.SWAP:
				self.send_signal(Signal.SWAP, 0)
			case Command.LCM:
				self.send_signal(Signal.LCM, 0)
			case Command.MOD:
				self.send_signal(Signal.MOD, 0)
			case Command.DIV:
				self.send_signal(Signal.DIV, 0)
			case Command.ADD:
				self.send_signal(Signal.ADD, 0)
			case Command.PUF:
				self.send_signal(Signal.PUF, 0)
			case Command.RETURN:
				self.send_signal(Signal.RETURN, 0)
				#self.data_path.in_int = 0
			case Command.INT:
				self.send_signal(Signal.INT, cur_instruction&0x00FF)
			case Command.DEC:
				self.send_signal(Signal.DEC, 0)
			case Command.INC:
				self.send_signal(Signal.INC, 0)
			case Command.EI:
				self.send_signal(Signal.EI, 0)
			case Command.MUL:
				self.send_signal(Signal.MUL, 0)
			case Command.NBL:
				self.send_signal(Signal.NBL, 0)
			case Command.BL:
				self.send_signal(Signal.BL, 0)
			case Command.EMPTY:
				self.send_signal(Signal.EMPTY, 0)
			case Command.RDPB:
				self.send_signal(Signal.RDPB, 0)
			case Command.RDPW:
				self.send_signal(Signal.RDPW, 0)
			case Command.WRPB:
				self.send_signal(Signal.WRPB, 0)
			case Command.WRPW:
				self.send_signal(Signal.WRPW, 0)				
		self.tickk()




def read_instr(file):
	interrupts = []
	instructions = []
	ro_data = []
	with open(file, "rb") as bin_f:
		ioadr = int.from_bytes(bin_f.read(1), 'big')
		for i in range(0, 8):
			interrupts.append(int.from_bytes(bin_f.read(1), 'big'))
		instr = 0 
		start_address = 8
		offset = 8
		cnt = 9
		while instr != b"":
			instr = bin_f.read(2)
			cnt += 2
			instructions.append(int.from_bytes(instr, 'big'))
			offset += 2
			if int.from_bytes(instr, 'big') == Command.RETURN:
				start_address = offset
			if int.from_bytes(instr, 'big') == 0:
				break
		for i in range(0, Offsets.RO_DATA_OFFSET-cnt+1):
			instr = bin_f.read(1)
			#print("READ:", instr)
		while instr != b"":
			instr = bin_f.read(8)
			#print("READ:", instr)
			if instr != b"":
				ro_data.append(instr)
		#print("INSTRS: ", instructions)
		#print("RODATA: ", ro_data)
	return interrupts, instructions, start_address, ro_data, ioadr



def read_inpt_tokens(inptkfn):
	input_tokens = []
	with open(inptkfn, "r", encoding="utf-8") as inp_file:
		lines = inp_file.readlines()
		for line in lines:
			words = line.split()
			assert len(words) == 2, "BAD INPUT FILE FORMAT!" 
			if words[1].isnumeric():
				input_tokens.append([int(words[0]), int(words[1])])
			else:
				ln = len(words[1])
				for j in range(ln, 8):
					words[1] += "\0"
				input_tokens.append([int(words[0]), words[1].encode('ascii')])
	return input_tokens



def main(args):
	#assert len(args) == 2 or len(args) == 1, "Wrong args: control_unit.py <binary_file> [<file_with_data>]"

	bin_file = args[0]
	data_file = args[1] if len(args) >= 2 else None
	out_file = args[2] if len(args) == 3 else None
	interrupts, instructions, start_addr, ro_data, ioadr = read_instr(bin_file)
	if data_file:
		input_tokens = read_inpt_tokens(data_file)
	else:
		input_tokens = []

	if out_file:
		outf = open(out_file, "w", encoding="utf-8")
	else:
		outf = 0

	#print(interrupts)
	#print(instructions)
	#print(start_addr)
	#print(ro_data)
	#print(ioadr)
	try:
		dp = Data_path(interrupts, instructions, start_addr, ro_data, ioadr, input_tokens, outf)
		cu = Control_Unit(dp)
		cu.start()
	except HaltProgram:
		pass


if __name__ == '__main__':
	main(sys.argv[1:])
