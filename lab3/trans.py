import enum
import sys
import struct



from lab3.common import Command, Offsets


symbol2command = {
	"halt": Command.HALT,
	"+": Command.ADD,
	"%": Command.MOD,
	"swap": Command.SWAP,
	"ztop": Command.ZTOP,
	"last": Command.LAST,
	".": Command.INT,
	"otop": Command.OTOP,
	"<<": Command.STORE,
	";": Command.RETURN,
	"pus": Command.PUS,
	"lcm": Command.LCM,
	"<": Command.PUSH_ARG,
	":": None,
	"^":Command.READ_ADR,
	"/": Command.DIV,
	"puf": Command.PUF,
	"dec": Command.DEC,
	"nop": Command.NOP,
	"ei": Command.EI,
	"di": Command.DI,
	"*": Command.MUL,
	"nbl": Command.NBL,
	"bl":Command.BL,
	"empty": Command.EMPTY,
	"rdpw": Command.RDPW,
	"rdpb": Command.RDPB,
	"wrpw": Command.WRPW,
	"wrpb": Command.WRPB,
	"di": Command.DI,
	"repeat": None,
	"begin": None,
	"while": None,
}

class Codes(enum.IntEnum):
	"""Коды, которые помогают провести бинарное кодирование
	"""
	PUSH_ARG_CODE = 0b0001000000000000
	JMP_CODE = 0b0000111000000000
	JZ_CODE = 0b0000110100000000
	CALL_CODE = 0b0000110000000000
	RET_CODE = 0b0000100100000000
	HALT_CODE = 0
	INT_CODE = 0b0000011000000000
	STORE_CODE = 0b0000100000000000
	READ_ADR_CODE = 0b0001000000000000
	DIV_CODE = 0b0001000100000000
	RO_DATA_START = 100

def dec_to_bin(x):
	return format(x, 'b').zfill(8)



def stringToBinary(string):
	ret = 0
	mul = 128
	for i in string:
		if i == '1':
			ret += mul
		mul /= 2
	return int(ret)


def translate(source):
	program_bin, mnemonics = [], []
	functions = {}
	interrupts = {}
	ro_tokens = {}
	ioadr = 0
	with open(source, "r", encoding="utf-8") as source_file:
		code = source_file.readlines()
	offset, jmp_pos, jz_pos, ro_data_offset = -1, -1, -1, Offsets.RO_DATA_OFFSET
	in_comment, in_function, in_interrupt = False, False, False
	for line in code:
		words = line.split()
		for word, next_word in zip(words, words[1:] + ['']):
			# print(word)
			if word == "(":
				in_comment = True
				# print(1)
				continue
			if word == ")":
				in_comment = False
				# print(2)
				continue
			if next_word == "$io":
				ioadr = int(word)
				continue
			if word == "$io":
				continue
			if word == ":":
				assert not in_function, "Function inside function!"
				in_function = True
				# print(3)
				continue
			if word not in symbol2command and next_word == "^":
				program_bin.append(dec_to_bin(Codes.READ_ADR_CODE>>8) + " " + dec_to_bin(int(word)))
				mnemonics.append("^ " + word)
				continue
			if word not in symbol2command and next_word == "<<":
				program_bin.append(dec_to_bin(Codes.STORE_CODE>>8) + " " + dec_to_bin(int(word)))
				mnemonics.append("<< " + word)
				continue
			if word == "begin":
				jmp_pos = offset + 1 + 4
				# print(4)
				continue
			if in_comment or word == "\n" or word == "<":
				# print(5)
				continue
			if word not in symbol2command and in_function and not next_word=="<<" and not next_word == "<":
				if(word == "int0" or word == "int1" or word == "int2" or word == "int3" or word == "int4" or word == "int5" or word == "int6" or word == "int7"):
					interrupts[word] = (offset+1)*2+8
				else:
					functions[word] = (offset + 1)*2 + 8
				# print(6)
				continue
			offset += 1
			if word == ".":
				program_bin.append(dec_to_bin(Codes.INT_CODE>>8) + " 00000001")
				mnemonics.append("int 1")
				# print(14)
				continue

			if word == "repeat":
				program_bin.append(dec_to_bin(Codes.JMP_CODE>>8) + " " + dec_to_bin(jmp_pos*2))
				mnemonics.append("jmp " + str(jmp_pos*2))
				program_bin[jz_pos] = dec_to_bin(Codes.JZ_CODE>>8) + " " + dec_to_bin(offset*2 + 2 + 8)
				mnemonics[jz_pos] = "jz " + str(offset*2 + 2 + 8)
				# print(7)
				continue
			if word == "while":
				program_bin.append("")
				mnemonics.append("")
				jz_pos = offset
				# print(8)
				continue
			if word == ";":
				in_function = False
				program_bin.append(dec_to_bin(Codes.RET_CODE>>8) + " 00000000")
				mnemonics.append("ret")
				# print(9)
				continue
			if word not in symbol2command and not in_function and not next_word == '<' and not next_word == "<<":
				program_bin.append(dec_to_bin(Codes.CALL_CODE>>8) + " " + dec_to_bin(functions[word]))
				mnemonics.append("call " + str(functions[word]))
				# print(10)
				continue
			if word not in symbol2command and next_word == '<':
				if word in ro_tokens:
					program_bin.append(dec_to_bin(Codes.PUSH_ARG_CODE>>8) + " " + dec_to_bin(ro_tokens[word]))
				else:
					ro_tokens[word] = ro_data_offset
					program_bin.append(dec_to_bin(Codes.PUSH_ARG_CODE>>8) + " " + dec_to_bin(ro_data_offset))
					ro_data_offset += 8
				mnemonics.append("< " + word)
				# print(11)
				continue
			if word == "<<" or word == "<" or word == "$io" or word == "^":
				continue
			program_bin.append(dec_to_bin(symbol2command.get(word).value>>8) + " 00000000")
			mnemonics.append(word)
			# print(12)
	program_bin.append(dec_to_bin(Codes.HALT_CODE) + " 00000000")
	mnemonics.append("halt")
	# print(13)




	# print(interrupts["int0"])
	# print(interrupts["int1"])
	# print(interrupts["int2"])
	# print(interrupts["int3"])
	# print(interrupts["int4"])
	# print(interrupts["int5"])
	# print(interrupts["int6"])
	# print(interrupts["int7"])
	# print(interrupts)
	# print("==========================")
	# print(program_bin)
	# print(mnemonics)

	return interrupts, program_bin, mnemonics, ioadr





def write_to_dbg(target, binary_program, mnemonics, ints, ioadr):
	with open(target, "w", encoding="utf-8") as target_file:
		target_file.write(str(dec_to_bin(ioadr))+"\n")
		if("int0" in ints):
			target_file.write(str(dec_to_bin(ints["int0"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int1" in ints):
			target_file.write(str(dec_to_bin(ints["int1"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int2" in ints):
			target_file.write(str(dec_to_bin(ints["int2"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int3" in ints):
			target_file.write(str(dec_to_bin(ints["int3"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int4" in ints):
			target_file.write(str(dec_to_bin(ints["int4"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int5" in ints):
			target_file.write(str(dec_to_bin(ints["int5"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))
		if("int6" in ints):
			target_file.write(str(dec_to_bin(ints["int6"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))    		    		
		if("int7" in ints):
			target_file.write(str(dec_to_bin(ints["int7"]) + "\n"))
		else:
			target_file.write(str("00000000\n"))    		    		    		 		    		
		itr = 0 		    		    		 		    		
		for i in binary_program:
			target_file.write(str(i))
			target_file.write(' ')
			target_file.write(str(mnemonics[itr]))
			target_file.write("\n")
			itr += 1

def write_to_bin(dbg, bin):
	dbg_f = open(dbg, "r", encoding="utf-8") 
	ro_data = []
	cnt = 0
	with open(bin, "wb") as bin_f:
		binstr = dbg_f.readline()
		bin_f.write(struct.pack('B', stringToBinary(binstr)))
		for i in range(0, 8):
			binstr = dbg_f.readline()
			bin_f.write(struct.pack('B', stringToBinary(binstr)))
			cnt += 1
		lines = dbg_f.readlines()
		for line in lines:
			words = line.split()
			if words[2] == "<":
				if words[3] not in ro_data:
					ro_data.append(words[3])
			bin_f.write(struct.pack('B', stringToBinary(words[0])))
			bin_f.write(struct.pack('B', stringToBinary(words[1])))
			cnt += 2
		for i in range(cnt, Offsets.RO_DATA_OFFSET):
			bin_f.write(int(0).to_bytes(1, byteorder = 'big'))
		for i in ro_data:
			if i.isnumeric():
				a = int(i)
				bin_f.write(a.to_bytes(8, byteorder = 'big'))
			else:
				ln = len(i)
				for j in range(ln, 8):
					i += "\0"
				bin_f.write(i.encode('ascii'))
	dbg_f.close()


def main(args):
	assert len(args) == 3, "Wrong arguments: translator.py <source_file> <target_file_dbg> <target_file>"

	source, target, bin = args
	ints, binary_program, mnemonics, ioadr = translate(source)
	write_to_dbg(target, binary_program, mnemonics, ints, ioadr)
	write_to_bin(target, bin)

if __name__ == '__main__':
	main(sys.argv[1:])
