import logging

from lab3.common import Signal, Offsets, instrnames, HaltProgram


class Data_path:

	def __init__(self, interrupts, instr, start_address, ro_data, ioadr, input_tokens, outfile):
		self.out = outfile
		self.in_int = 0
		self.ioadr = ioadr

		self.memory = interrupts #+ instr + [0] * (Offsets.RO_DATA_OFFSET - len(instr)*2 - 8) + ro_data + [0] * (Offsets.MEMORY_GENERAL_SIZE-Offsets.RO_DATA_OFFSET - len(ro_data)*8)

		for i in instr:
			self.memory.append(i>>8)
			self.memory.append(i&0x00FF)

		for i in range(0, Offsets.RO_DATA_OFFSET - len(instr)*2 - 8):
			self.memory.append(0)


		for i in ro_data:
			self.memory.append(int(bytearray(i)[0]))
			self.memory.append(int(bytearray(i)[1]))
			self.memory.append(int(bytearray(i)[2]))
			self.memory.append(int(bytearray(i)[3]))
			self.memory.append(int(bytearray(i)[4]))
			self.memory.append(int(bytearray(i)[5]))
			self.memory.append(int(bytearray(i)[6]))
			self.memory.append(int(bytearray(i)[7]))

		for i in range(0, Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET - len(ro_data)*8):
			self.memory.append(0)


		self.stack = []
		self.ret_stack = []
		self.sp = -1

		self.pc = start_address

		self.inp_token = input_tokens
		self.output_tokens = []

		#print(self.memory, len(self.memory))


	def check_tick(self, tick):
		if len(self.inp_token) == 0:
			return 0, 0
		if(self.inp_token[0][0] <= tick):
			return 1, self.inp_token.pop(0)[1]
		else:
			return 0, 0


	def latch_pc(self):
		self.pc += 2 # ?
		if self.pc >= Offsets.RO_DATA_OFFSET:
			raise segovfwl()

	def pop_stack(self):
		self.stack.pop()
		self.sp -= 1

	def push_end_to_stack(self):
		self.stack.append(0)
		self.stack_pointer += 1

	def get_next_instruction(self, tick):
		fb = self.memory[self.pc-2]<<8
		sb = self.memory[self.pc-1]
		print("TICK: ", tick, " FETCHED: ", instrnames[fb], sb, '0x%04x'%(fb+sb), " PC: ", self.pc, " STACK: ", end="")#hex(fb+sb))
		for i in self.stack:
			print(i, end=" ")
		print("< TOP")
		return fb+sb

	def handle_signal(self, signal, addr):
		match signal:
			case Signal.WRITE:
				val = self.stack.pop()
				#print("VAL: ", val)
				barr = bytearray(val.to_bytes(8, 'big'))
				self.memory[addr] = barr[0]
				self.memory[addr+1] = barr[1]
				self.memory[addr+2] = barr[2]
				self.memory[addr+3] = barr[3]
				self.memory[addr+4] = barr[4]
				self.memory[addr+5] = barr[5]
				self.memory[addr+6] = barr[6]
				self.memory[addr+7] = barr[7]
				self.sp -= 1
				if(addr == self.ioadr):
					if self.out:
						outstr = ""
						flg = 0
						for i in range(0, 8):
							if barr[i] != 0:
								outstr += chr(barr[i])
								flg = 0
						self.out.write(outstr + "\n")
			case Signal.CHECK_ONE:
				try:
					check_ans = int(int(self.stack[self.sp]) != 1) if self.stack else 1
				except ValueError:
					check_ans = 1
				self.stack.append(check_ans)
				self.sp += 1
			case Signal.CHECK_NOT_ZERO:
				try:
					check_ans = int(int(self.stack[self.sp]) != 0) if self.stack else 1
				except ValueError:
					check_ans = 1
				self.stack.append(check_ans)
				self.sp += 1
			case Signal.CHECK_NOT_LAST:
				if len(self.stack) == 1:
					check_ans = 0
				else:
					check_ans = 1
				self.stack.append(check_ans)
				self.sp += 1
			case Signal.SWAP:
				#print("SP: ", self.sp, " Swapping ", self.sp - 1, self.stack[self.sp])
				self.stack.insert(self.sp - 1, self.stack[self.sp])
				self.sp += 1
			case Signal.MOD:
				op1 = int(self.stack.pop())
				op2 = int(self.stack.pop())
				self.stack.append(op2%op1)
				self.sp -= 1
			case Signal.ADD:
				self.stack.append(int(self.stack.pop()) + int(self.stack.pop()))
				self.sp -= 1
			case Signal.READ:
				resb1 = self.memory[addr]<<56
				resb2 = self.memory[addr+1]<<48
				resb3 = self.memory[addr+2]<<40
				resb4 = self.memory[addr+3]<<32
				resb5 = self.memory[addr+4]<<24
				resb6 = self.memory[addr+5]<<16
				resb7 = self.memory[addr+6]<<8 
				resb8 = self.memory[addr+7]
				#print("READVAL: ", resb1, resb2, resb3, resb4, resb5, resb6, resb7, resb8, "RESV: ", resb1 + resb2 + resb3 + resb4 + resb5 + resb6 + resb7 + resb8)
				self.stack.append(resb1 + resb2 + resb3 + resb4 + resb5 + resb6 + resb7 + resb8)
				self.sp += 1
			case Signal.PUS:
				if self.sp > 0:
					self.stack.append(self.stack[self.sp-1])
					self.sp += 1
			case Signal.LCM:
				gcd = int(self.stack.pop())
				first = int(self.stack.pop())
				second = int(self.stack.pop())
				self.stack.append(first * second // gcd)
				self.sp -= 2
			case Signal.DIV:
				op1 = int(self.stack.pop())
				op2 = int(self.stack.pop())
				self.stack.append(int(op2/op1))
				self.sp -= 1
			case Signal.PUF:
					self.stack.append(self.stack[self.sp])
					self.sp += 1
			case Signal.DEC:
				self.stack[self.sp] -= 1
			case Signal.INC:
				self.stack[self.sp] += 1				
			case Signal.EI:
				self.in_int = 0
			case Signal.MUL:
				op1 = int(self.stack.pop())
				op2 = int(self.stack.pop())
				self.stack.append(int(op2*op1))
				self.sp -= 1	
			case Signal.NBL:
				self.stack[self.sp] = int(not bool(self.stack[self.sp]))
			case Signal.BL:
				self.stack[self.sp] = int(bool(self.stack[self.sp]))
			case Signal.EMPTY:
				if(len(self.stack) == 0):
					self.stack.append(0)
					self.sp += 1
				else:
					self.stack.append(1)
					self.sp += 1
			case Signal.HALT:
				raise HaltProgram()
			case Signal.JZ:
				if not bool(self.stack[self.sp]):
					self.pc = addr&0x00FF
				self.pop_stack()
			case Signal.JUMP:
				self.pc = addr&0x00FF
			case Signal.CALL:
				self.ret_stack.append(self.pc)
				self.pc = addr&0x00FF
			case Signal.RETURN:
				self.pc = self.ret_stack.pop()
			case Signal.INT:
				self.ret_stack.append(self.pc)
				self.pc = self.memory[addr&0x00FF]
			case Signal.RDPB:
				ptr = self.stack.pop()
				self.stack.append(self.memory[ptr])
				self.stack.append(ptr)
				self.sp += 1
				
			case Signal.RDPW:
				ptr = self.stack.pop()
				resb1 = self.memory[ptr]<<56
				resb2 = self.memory[ptr+1]<<48
				resb3 = self.memory[ptr+2]<<40
				resb4 = self.memory[ptr+3]<<32
				resb5 = self.memory[ptr+4]<<24
				resb6 = self.memory[ptr+5]<<16
				resb7 = self.memory[ptr+6]<<8 
				resb8 = self.memory[ptr+7]
				#print("READVAL: ", resb1, resb2, resb3, resb4, resb5, resb6, resb7, resb8, "RESV: ", resb1 + resb2 + resb3 + resb4 + resb5 + resb6 + resb7 + resb8)
				self.stack.append(resb1 + resb2 + resb3 + resb4 + resb5 + resb6 + resb7 + resb8)
				self.stack.append(ptr)
				self.sp += 1

			case Signal.WRPW:
				ptr = self.stack.pop()
				val = self.stack.pop()
				#print("VAL: ", val)
				barr = bytearray(val.to_bytes(8, 'big'))
				self.memory[ptr] = barr[0]
				self.memory[ptr+1] = barr[1]
				self.memory[ptr+2] = barr[2]
				self.memory[ptr+3] = barr[3]
				self.memory[ptr+4] = barr[4]
				self.memory[ptr+5] = barr[5]
				self.memory[ptr+6] = barr[6]
				self.memory[ptr+7] = barr[7]
				self.stack.append(ptr)
				self.sp -= 1				

			case Signal.WRPB:
				ptr = self.stack.pop()
				wrt = self.stack.pop() & 0x00FF
				self.memory[ptr] = wrt
				self.stack.append(ptr)
				self.sp -= 1