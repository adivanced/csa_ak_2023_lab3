import enum

class Command(enum.IntEnum):
    """Команды, которые могут прийти в CU
    """          
    HALT     = 0b0000000000000000
    ADD      = 0b0000000100000000
    MOD      = 0b0000001000000000
    SWAP     = 0b0000001100000000
    ZTOP     = 0b0000010000000000
    LAST     = 0b0000010100000000
    INT      = 0b0000011000000000
    OTOP     = 0b0000011100000000
    STORE    = 0b0000100000000000
    RETURN   = 0b0000100100000000
    PUS      = 0b0000101000000000
    LCM      = 0b0000101100000000
    CALL     = 0b0000110000000000
    JZ       = 0b0000110100000000
    JUMP     = 0b0000111000000000
    PUSH_ARG = 0b0001000000000000
    READ_ADR = 0b0001000000000000
    DIV      = 0b0001000100000000
    PUF      = 0b0001001000000000
    DEC      = 0b0001001100000000
    EI       = 0b0001010000000000
    MUL      = 0b0001010100000000
    NBL      = 0b0001011000000000
    BL       = 0b0001100000000000
    EMPTY    = 0b0001100100000000
    RDPW     = 0b0001101000000000
    RDPB     = 0b0001101100000000
    WRPW     = 0b0001110000000000
    WRPB     = 0b0001110100000000
    INC      = 0b0001111000000000
    DI       = 0b0001111100000000
    NOP      = 0b1111111100000000



instrnames = {
    Command.HALT: "halt", 
    Command.ADD: "add", 
    Command.MOD: "mod", 
    Command.SWAP: "swap", 
    Command.ZTOP: "ztop", 
    Command.LAST: "last", 
    Command.INT: "int", 
    Command.OTOP: "otop", 
    Command.STORE: "store", 
    Command.RETURN: "ret", 
    Command.PUS: "pus", 
    Command.LCM: "lcm", 
    Command.CALL: "call", 
    Command.JZ: "jz", 
    Command.JUMP: "jmp", 
    Command.READ_ADR: "read",
    Command.DIV: "div",
    Command.PUF: "puf",
    Command.DEC: "dec",
    Command.EI: "ei",
    Command.MUL: "mul",
    Command.NBL: "nbl",
    Command.BL: "bl",
    Command.EMPTY: "empty",
    Command.NOP: "nop",
    Command.DI: "di",
    Command.INC: "inc",
    Command.WRPB: "wrpb",
    Command.WRPW: "wrpw",
    Command.RDPB: "rdpb",
    Command.RDPW: "rdpw"
}




class Offsets(enum.IntEnum):
    INTERRUPT_OFFSET = 0
    INSTRUCTIONS_OFFSET = 8
    RO_DATA_OFFSET = 100
    DATA_OFFSET = 164
    MEMORY_GENERAL_SIZE = 256

class Signal(enum.IntEnum):
    WRITE = 0
    CHECK_ONE = 1
    CHECK_NOT_ZERO = 2
    CHECK_NOT_LAST = 3
    SWAP = 4
    MOD = 5
    ADD = 6
    READ = 7
    PUS = 8
    LCM = 9
    DIV = 10
    PUF = 11
    DEC = 12
    EI = 13
    MUL = 14
    NBL = 15
    BL = 16
    EMPTY = 17
    HALT = 18
    JZ = 19
    JUMP = 20
    CALL = 21
    RETURN = 22
    INT = 23
    DI = 24
    RDPW = 25
    RDPB = 26
    WRPW = 27
    WRPB = 28
    INC = 29
    NOP = 50



class HaltProgram(Exception):
    """Вызывается для остановки процессора
    """


class InputTokensEnd(Exception):
    """Вызывается для остановки считывания символов с потока
    """


class SegmentOverflow(Exception):
    """Вызывается для остановки программы, если выходим за пределы сегмента
    """
