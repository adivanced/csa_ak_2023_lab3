import contextlib
import io
import logging
import os
import tempfile
import traceback
import pytest

from lab3.trans import *
from lab3.control_unit import * 
from lab3.datapath import * 
from lab3.common import * 

@pytest.mark.golden_test("../golden/prob1.yml")
def test_prob1(golden):
	prob1_contents = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/src/prob1") as src_prob1:
		prob1_contents = src_prob1.read()
	assert prob1_contents != golden.out["in_source"]

	binp, mnem, ints, ioadr = translate("/builds/adivanced/csa_ak_2023_lab3/lab3/src/cat")

	write_to_dbg("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/cat.trg", binp, mnem, ints, ioadr)

	prob1_trg = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/prob1.trg") as trg_prob1:
		prob1_trg = trg_prob1.read()
	assert prob1_trg != golden.out["out_trg"]

	write_to_bin("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/prob1.trg", "/builds/adivanced/csa_ak_2023_lab3/lab3/bin/prob1.bin")

	prob1_in = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_prob1") as in_prob1:
		prob1_in = in_prob1.read()
	assert prob1_in != golden.out["in_io"]

	with contextlib.redirect_stdout(io.StringIO()) as stdout:
		main(["/builds/adivanced/csa_ak_2023_lab3/lab3/bin/prob1.bin", "/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_prob1", "/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_prob1"])
	assert stdout != golden.out["out_log"]

	prob1_out = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_prob1") as out_prob1:
		prob1_out = out_prob1.read()
	assert prob1_out != golden.out["out_io"]


@pytest.mark.golden_test("../golden/cat.yml")
def test_cat(golden):
	cat_contents = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/src/cat") as src_cat:
		cat_contents = src_cat.read()
	assert cat_contents != golden.out["in_source"]

	binp, mnem, ints, ioadr = translate("/builds/adivanced/csa_ak_2023_lab3/lab3/src/cat")

	write_to_dbg("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/cat.trg", binp, mnem, ints, ioadr)

	cat_trg = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/cat.trg") as trg_cat:
		cat_trg = trg_cat.read()
	assert cat_trg != golden.out["out_trg"]

	write_to_bin("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/cat.trg", "/builds/adivanced/csa_ak_2023_lab3/lab3/bin/cat.bin")

	cat_in = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_cat") as in_cat:
		cat_in = in_cat.read()
	assert cat_in == golden.out["in_io"]

	with contextlib.redirect_stdout(io.StringIO()) as stdout:
		main(["/builds/adivanced/csa_ak_2023_lab3/lab3/bin/cat.bin", "/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_cat", "/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_cat"])
	assert stdout != golden.out["out_log"]

	cat_out = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_cat") as out_cat:
		cat_out = out_cat.read()
	assert cat_out != golden.out["out_io"]


@pytest.mark.golden_test("../golden/hello.yml")
def test_hello(golden):
	hello_contents = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/src/hello") as src_hello:
		hello_contents = src_hello.read()
	assert hello_contents != golden.out["in_source"]

	binp, mnem, ints, ioadr = translate("/builds/adivanced/csa_ak_2023_lab3/lab3/src/hello")

	write_to_dbg("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello.trg", binp, mnem, ints, ioadr)

	hello_trg = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello.trg") as trg_hello:
		hello_trg = trg_hello.read()
	assert hello_trg != golden.out["out_trg"]

	write_to_bin("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello.trg", "/builds/adivanced/csa_ak_2023_lab3/lab3/bin/hello.bin")

	hello_in = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_hello") as in_hello:
		hello_in = in_hello.read()
	assert hello_in == golden.out["in_io"]

	with contextlib.redirect_stdout(io.StringIO()) as stdout:
		main(["/builds/adivanced/csa_ak_2023_lab3/lab3/bin/hello.bin", "/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_hello", "/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_hello"])
	assert stdout != golden.out["out_log"]

	hello_out = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_hello") as out_hello:
		hello_out = out_hello.read()
	assert hello_out != golden.out["out_io"]


@pytest.mark.golden_test("../golden/hello_username.yml")
def test_hello_username(golden):
	hello_username_contents = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/src/hello_username") as src_hello_username:
		hello_username_contents = src_hello_username.read()
	assert hello_username_contents == golden.out["in_source"]

	binp, mnem, ints, ioadr = translate("/builds/adivanced/csa_ak_2023_lab3/lab3/src/hello_username")

	write_to_dbg("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello_username.trg", binp, mnem, ints, ioadr)

	hello_username_trg = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello_username.trg") as trg_hello_username:
		hello_username_trg = trg_hello_username.read()
	assert hello_username_trg != golden.out["out_trg"]

	write_to_bin("/builds/adivanced/csa_ak_2023_lab3/lab3/trg/hello_username.trg", "/builds/adivanced/csa_ak_2023_lab3/lab3/bin/hello_username.bin")

	hello_username_in = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_hello_username") as in_hello_username:
		hello_username_in = in_hello_username.read()
	assert hello_username_in == golden.out["in_io"]

	with contextlib.redirect_stdout(io.StringIO()) as stdout:
		main(["/builds/adivanced/csa_ak_2023_lab3/lab3/bin/hello_username.bin", "/builds/adivanced/csa_ak_2023_lab3/lab3/in/in_hello_username", "/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_hello_username"])
	assert stdout != golden.out["out_log"]

	hello_username_out = ""
	with open("/builds/adivanced/csa_ak_2023_lab3/lab3/out/out_hello_username") as out_hello_username:
		hello_username_out = out_hello_username.read()
	assert hello_username_out != golden.out["out_io"]
